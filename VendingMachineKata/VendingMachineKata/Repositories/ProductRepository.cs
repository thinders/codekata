﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VendingMachineKata.Models;

namespace VendingMachineKata.Repositories
{
    public interface IProductRepository
    {
        List<Product> GetAvailableProducts();
    }

    public class ProductRepository : IProductRepository
    {
        public List<Product> GetAvailableProducts()
        {
            var cola = new Product { Value = 1m, Name = "Cola", ProductNumber = 1 };
            var chips = new Product { Value = .5m, Name = "Chips", ProductNumber = 2 };
            var candy = new Product { Value = .65m, Name = "Candy", ProductNumber = 3 };
            return new List<Product> { cola ,chips,candy};
        }
    }
}