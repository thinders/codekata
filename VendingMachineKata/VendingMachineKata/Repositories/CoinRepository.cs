﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VendingMachineKata.Models;

namespace VendingMachineKata.Repositories
{
    public interface ICoinRepository
    {
        List<Coin> GetValidCoins();
    }

    public class CoinRepository: ICoinRepository
    {
        public List<Coin> GetValidCoins()
        {
            var nickel = new Coin(3, 3, .05m, Constants.NICKEL);
            var dime = new Coin(1, 1, .10m, Constants.DIME);
            var quarter = new Coin(4, 4, .25m, Constants.QUARTER);
            return new List<Coin> { nickel, dime, quarter };
        }
    }

    public static class Constants
    {
        public const string QUARTER = "Quarter";
        public const string DIME = "Dime";
        public const string NICKEL = "Nickel";
    }
}