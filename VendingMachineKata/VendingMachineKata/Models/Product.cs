﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingMachineKata.Models
{
    public class Product
    {
        public decimal Value { get; set; }
        public string Name { get; set; }
        public int ProductNumber { get; set; }
    }
}