﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingMachineKata.Models
{
    public class VendingMachineViewModel
    {
        public int Weight { get; set; }
        public int Size { get; set; }

        public decimal Amount { get; set; }
        public string AmountAsString
        {
            get
            {
                if (Amount == 0) return "INSERT COIN";
                return Amount.ToString();
            }
        }

        public int SelectedProductNumber { get; set; }
        public string Display { get; set; }
        
        public List<Coin> CoinReturn;
        public Product Product { get; set; }

        public VendingMachineViewModel()
        {
            CoinReturn = new List<Coin>();
        }
    }
}