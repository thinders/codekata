﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VendingMachineKata.Models
{
    public class Coin
    {
        public int Size { get; set; }
        public int Weight { get; set; }
        public decimal Value { get; set; }
        public string Name { get; set; }

        public Coin(int size, int weight, decimal value, string name)
        {
            Size = size;
            Weight = weight;
            Value = value;
            Name = name;
        }
    }
}