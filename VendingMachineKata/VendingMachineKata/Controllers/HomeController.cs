﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VendingMachineKata.Models;
using VendingMachineKata.Services;

namespace VendingMachineKata.Controllers
{
    public class HomeController : Controller
    {
        private VendingMachineService _vendingMachineService;

        public HomeController()
        {
            _vendingMachineService = new VendingMachineService();
        }

        public ActionResult Index()
        {
            return View(new VendingMachineViewModel());
        }
        
        [HttpPost]
        public ActionResult AddCoin(VendingMachineViewModel model)
        {
            _vendingMachineService.AddCoin(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult SelectProduct(VendingMachineViewModel model)
        {
            return View(model);
        }
    }
}