﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VendingMachineKata.Models;
using VendingMachineKata.Repositories;

namespace VendingMachineKata.Services
{
    public class VendingMachineService
    {
        private ICoinRepository _coinRepo;
        private List<Coin> ValidCoins;

        private IProductRepository _productRepo;
        private List<Product> Products;
        
        public VendingMachineService()
        {
            _coinRepo = new CoinRepository();
            _productRepo = new ProductRepository();

            ValidCoins = _coinRepo.GetValidCoins().OrderBy(x => x.Value).ToList();
            Products = _productRepo.GetAvailableProducts();
        }
        
        public void AddCoin(VendingMachineViewModel model)
        {
            var coinValue = AddCoin(model.Size, model.Weight);
            if(coinValue == 0)
                model.CoinReturn.Add(new Coin(model.Size, model.Weight, 0, ""));

            model.Amount += coinValue;
            if (model.Amount == 0)
            {
                model.Display = "INSERT COIN";
                return;
            }

            model.Display = string.Format("{0:C}", model.Amount);
        }

        public decimal AddCoin(int size, int weight)
        {
            var validCoin = ValidCoins.SingleOrDefault(x => x.Size == size && x.Weight == weight);

            if (validCoin == null)
                return 0;

            return validCoin.Value;
        }

        public void SelectProduct(VendingMachineViewModel model)
        {
            model.Product = null;
            var productSelected = model.SelectedProductNumber;
            var product = Products.SingleOrDefault(x => x.ProductNumber == productSelected);

            if (product == null)
            {
                model.Display = "INVALID PRODUCT";
                return;
            }
            
            if(model.Amount < product.Value)
            {
                model.Display = "PRICE: " + string.Format("{0:C}", product.Value);
                return;
            }

            model.Product = product;

            MakeChange(model);

            model.Display = "THANK YOU";
            model.Amount = 0;
        }

        public void MakeChange(VendingMachineViewModel model)
        {
            if (model.Amount == model.Product.Value)
                return;

            var amountToReturn = model.Amount - model.Product.Value;
            while (amountToReturn > 0)
            {
                if (amountToReturn >= .25m)
                {
                    amountToReturn -= MakeChange(model, Constants.QUARTER);
                    continue;
                }

                if (amountToReturn >= .1m)
                {
                    amountToReturn -= MakeChange(model, Constants.DIME);
                    continue;
                }

                if (amountToReturn >= .05m)
                {
                    amountToReturn -= MakeChange(model, Constants.NICKEL);
                    continue;
                }

                // shouldn't be possible
                break;
            }
        }

        private decimal MakeChange(VendingMachineViewModel model,string coinName)
        {
            var coin = ValidCoins.Single(x => x.Name == coinName);
            model.CoinReturn.Add(coin);
            return coin.Value;
        }
    }
}