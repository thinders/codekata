﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachineKata.Services;
using VendingMachineKata.Models;
using System.Linq;
using VendingMachineKata.Repositories;

namespace UnitTest.Services.VendingMachineServiceTests
{
    [TestClass]
    public class MakeChangeTests
    {
        private VendingMachineService _vendingMachine;

        [TestInitialize]
        public void GetItStarted()
        {
            _vendingMachine = new VendingMachineService();
        }

        [TestMethod]
        public void MakeChange_NoMoneyLeftover_ReturnNoCoins()
        {
            var model = new VendingMachineViewModel
            {
                Amount = .65m,
                SelectedProductNumber = 1,
                Product = new Product { Value = .65m}
            };

            _vendingMachine.MakeChange(model);

            Assert.IsTrue(model.CoinReturn.Count == 0);
        }

        [TestMethod]
        public void MakeChange_QuarterLeftover_ReturnQuarter()
        {
            var model = new VendingMachineViewModel
            {
                Amount = .75m,
                SelectedProductNumber = 1,
                Product = new Product { Value = .50m }
            };

            _vendingMachine.MakeChange(model);

            Assert.IsTrue(model.CoinReturn.Count == 1);
            Assert.AreEqual(.25m, model.CoinReturn.First().Value);
        }

        [TestMethod]
        public void MakeChange_DimeLeftover_ReturnDime()
        {
            var model = new VendingMachineViewModel
            {
                Amount = .60m,
                SelectedProductNumber = 1,
                Product = new Product { Value = .50m }
            };

            _vendingMachine.MakeChange(model);

            Assert.IsTrue(model.CoinReturn.Count == 1);
            Assert.AreEqual(.1m, model.CoinReturn.First().Value);
        }

        [TestMethod]
        public void MakeChange_NickelLeftover_ReturnNickel()
        {
            var model = new VendingMachineViewModel
            {
                Amount = .55m,
                SelectedProductNumber = 1,
                Product = new Product { Value = .50m }
            };

            _vendingMachine.MakeChange(model);

            Assert.IsTrue(model.CoinReturn.Count == 1);
            Assert.AreEqual(.05m, model.CoinReturn.First().Value);
        }

        [TestMethod]
        public void MakeChange_FortyCentsLeftOver_ReturnQuarterDimeNickel()
        {
            var model = new VendingMachineViewModel
            {
                Amount = .90m,
                SelectedProductNumber = 1,
                Product = new Product { Value = .50m }
            };

            _vendingMachine.MakeChange(model);

            Assert.IsTrue(model.CoinReturn.Count == 3);
            Assert.IsTrue(model.CoinReturn.Sum(x => x.Value) == .4m);
            Assert.IsNotNull(model.CoinReturn.SingleOrDefault(x => x.Name == Constants.QUARTER));
            Assert.IsNotNull(model.CoinReturn.SingleOrDefault(x => x.Name == Constants.DIME));
            Assert.IsNotNull(model.CoinReturn.SingleOrDefault(x => x.Name == Constants.NICKEL));
        }
    }
}
