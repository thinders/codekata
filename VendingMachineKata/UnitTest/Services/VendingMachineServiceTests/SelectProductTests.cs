﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachineKata.Services;
using VendingMachineKata.Models;
using System.Linq;

namespace UnitTest.Services.VendingMachineServiceTests
{
    [TestClass]
    public class SelectProductTests
    {
        private VendingMachineService _vendingMachine;

        [TestInitialize]
        public void GetItStarted()
        {
            _vendingMachine = new VendingMachineService();
        }

        [TestMethod]
        public void SelectProduct_VendorSelectsInvalidOption_DisplayInvalid()
        {
            var model = new VendingMachineViewModel
            {
                Amount = 0,
                SelectedProductNumber = 8
            };
            _vendingMachine.SelectProduct(model);

            Assert.AreEqual("INVALID PRODUCT",model.Display);
        }

        [TestMethod]
        public void SelectProduct_VendorSelectsOption1WithNoMoney_DisplayPriceOfItem()
        {
            var model = new VendingMachineViewModel
            {
                Amount = 0,
                SelectedProductNumber = 1
            };
            _vendingMachine.SelectProduct(model);

            Assert.AreEqual("PRICE: $1.00", model.Display);
        }

        [TestMethod]
        public void SelectProduct_VendorSelectsOption1WithNotEnoughMoney_DisplayPriceOfItem()
        {
            var model = new VendingMachineViewModel
            {
                Amount = .95m,
                SelectedProductNumber = 1
            };
            _vendingMachine.SelectProduct(model);

            Assert.AreEqual("PRICE: $1.00", model.Display);
        }

        [TestMethod]
        public void SelectProduct_VendorSelectsOption1WithNotEnoughMoney_ProductIsNotGiven()
        {
            var model = new VendingMachineViewModel
            {
                Amount = .95m,
                SelectedProductNumber = 1
            };
            _vendingMachine.SelectProduct(model);

            Assert.IsNull(model.Product);
        }

        [TestMethod]
        public void SelectProduct_VendorSelectsOption1WithExactMoney_DisplayThankYou()
        {
            var model = new VendingMachineViewModel
            {
                Amount = 1m,
                SelectedProductNumber = 1
            };
            _vendingMachine.SelectProduct(model);

            Assert.AreEqual("THANK YOU", model.Display);
        }

        [TestMethod]
        public void SelectProduct_VendorSelectsOption1WithExactMoney_AmountIsSetToZero()
        {
            var model = new VendingMachineViewModel
            {
                Amount = 1m,
                SelectedProductNumber = 1
            };
            _vendingMachine.SelectProduct(model);

            Assert.AreEqual(0, model.Amount);
        }

        [TestMethod]
        public void SelectProduct_VendorSelectsOption1WithExactMoney_ProductIsGiven()
        {
            var model = new VendingMachineViewModel
            {
                Amount = 1m,
                SelectedProductNumber = 1
            };
            _vendingMachine.SelectProduct(model);

            Assert.IsNotNull(model.Product);
            Assert.AreEqual("Cola", model.Product.Name);
            Assert.AreEqual(1, model.Product.ProductNumber);
        }

        [TestMethod]
        public void SelectProduct_ChangeIsGiven()
        {
            var model = new VendingMachineViewModel
            {
                Amount = .55m,
                SelectedProductNumber = 2
            };
            _vendingMachine.SelectProduct(model);

            Assert.IsTrue(model.CoinReturn.Count == 1);
            Assert.AreEqual(.05m, model.CoinReturn.First().Value);
        }
    }
}
