﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using VendingMachineKata.Models;
using VendingMachineKata.Services;

namespace UnitTest.Services.VendingMachineServiceTests
{
    [TestClass]
    public class AddCoinTests
    {
        private VendingMachineService _vendingMachine;

        [TestInitialize]
        public void GetItStarted()
        {
            _vendingMachine = new VendingMachineService();
        }
        
        [TestMethod]
        public void AddCoin_VendorInsertsAnInvalidCoin_RetursZero()
        {
            var amount = _vendingMachine.AddCoin(0, 1);
            Assert.AreEqual(0m, amount);
        }

        [TestMethod]
        public void AddCoin_VendorInsertsANickel_ReturnFive()
        {
            var amount = _vendingMachine.AddCoin(3,3);
            Assert.AreEqual(.05m, amount);
        }

        [TestMethod]
        public void AddCoin_VendorInsertsADime_ReturnTen()
        {
            var amount = _vendingMachine.AddCoin(1, 1);
            Assert.AreEqual(.1m, amount);
        }
        [TestMethod]
        public void AddCoin_VendorInsertsAQuarter_ReturnTwentyFive()
        {
            var amount = _vendingMachine.AddCoin(4, 4);
            Assert.AreEqual(.25m, amount);
        }

        [TestMethod]
        public void AddCoin_InvalidCoin_DisplayInsertCoin()
        {
            var model = new VendingMachineViewModel
            {
                Weight = 2,
                Size = 2
            };
            _vendingMachine.AddCoin(model);

            Assert.AreEqual("INSERT COIN", model.Display);
        }

        [TestMethod]
        public void AddCoin_InvalidCoin_PutCoinInCoinReturn()
        {
            var model = new VendingMachineViewModel
            {
                Weight = 2,
                Size = 2
            };
            _vendingMachine.AddCoin(model);

            Assert.AreEqual(model.CoinReturn.Count, 1);
            Assert.AreEqual(model.CoinReturn.First().Weight, 2);
            Assert.AreEqual(model.CoinReturn.First().Size, 2);
            Assert.AreEqual(model.CoinReturn.First().Value, 0);
        }

        [TestMethod]
        public void AmountAsString_AddNickel_DisplayFiveCents()
        {
            var model = new VendingMachineViewModel
            {
                Weight = 3,
                Size = 3
            };
            _vendingMachine.AddCoin(model);

            Assert.AreEqual("$0.05", model.Display);
        }

        [TestMethod]
        public void AmountAsString_AddCoins_DisplaySum()
        {
            var model = new VendingMachineViewModel
            {
                Weight = 3,
                Size = 3
            };
            _vendingMachine.AddCoin(model);

            model.Weight = 1;
            model.Size = 1;
            _vendingMachine.AddCoin(model);

            model.Weight = 4;
            model.Size = 4;
            _vendingMachine.AddCoin(model);

            Assert.AreEqual("$0.40", model.Display);
        }
    }
}
